The ISiS software is a command line application for singing synthesis that can be used to generate
singing signals by means of synthesizing them from melody and lyrics.

The ISiS software is the result of the French national project [ChaNTeR](https://chanter.lam.jussieu.fr/doku.php)
([Project ANR-13-CORD-011](https://anr.fr/Projet-ANR-13-CORD-0011)),
that was performed in collaboration with Acapela, LIMSI, and Dualo. ISiS is distributed free of
charge for members of the IRCAM Forum [here](http://forumnet.ircam.fr/product/isis/).

ISiS operates offline reading score and lyrics from file and renders the results into an output
sound file. In its current version ISiS supports synthesis with 3 French singing voices:

    RT: a tenor male pop singer, and
    MS: a female mezzo-soprano pop singer, and
    EL: a female soprano lyrical singer.

Basic documentation is available [here](https://isis-documentation.readthedocs.io/en/latest/).

## ChangeLog

### ISiS Version 1.2.6

  many bug fixes, first release for Linux.

### ISiS Version 1.2.5

 does no longer require to use shell environment variables for voice configuration, which  simplifies
 running the ISiS software  under Max/MSP.