.. _isis-xsampa-examples:


## XSAMPA Examples

The following list provides examples for each XSAMPA symbol that is supported in _ISiS_:

### Vowels

 | XSAMPA Symbol  | API  | french word | list of words | explanation |          
 |----------------| ------| ---------------|-------------------|-------------------|
 | a  | a |  l<b>a</b>  | [examples](https://fr.wikipedia.org/wiki/Liste_des_graphies_des_phon%C3%A8mes_du_fran%C3%A7ais#/a/) | [further reading](https://fr.wikipedia.org/wiki/Voyelle_ouverte_ant%C3%A9rieure_non_arrondie) |
 | e  |  e | beaut<b>é</b> | [examples](https://fr.wikipedia.org/wiki/Liste_des_graphies_des_phon%C3%A8mes_du_fran%C3%A7ais#/e/) |[further reading](https://fr.wikipedia.org/wiki/Voyelle_mi-ferm%C3%A9e_ant%C3%A9rieure_non_arrondie)|
 | E  | ɛ | c<b>è</b>pe| [examples](https://fr.wikipedia.org/wiki/Liste_des_graphies_des_phon%C3%A8mes_du_fran%C3%A7ais#/%C9%9B/) |[further reading](https://fr.wikipedia.org/wiki/Voyelle_mi-ouverte_ant%C3%A9rieure_non_arrondie)| 
 | 2  | ø |  p<b>eu</b>| [examples](https://fr.wikipedia.org/wiki/Liste_des_graphies_des_phon%C3%A8mes_du_fran%C3%A7ais#/%C3%B8/)|[further reading](https://fr.wikipedia.org/wiki/Voyelle_mi-ferm%C3%A9e_ant%C3%A9rieure_arrondie)|
 | 9  | œ |  j<b>eu</b>ne |[examples](https://fr.wikipedia.org/wiki/Liste_des_graphies_des_phon%C3%A8mes_du_fran%C3%A7ais#/%C5%93/)| [further reading](https://fr.wikipedia.org/wiki/Voyelle_mi-ouverte_ant%C3%A9rieure_arrondie)|
 | @  | ə | m<b>e</b>nu | [examples](https://fr.wikipedia.org/wiki/Liste_des_graphies_des_phon%C3%A8mes_du_fran%C3%A7ais#/%C9%99/)|[further reading](https://fr.wikipedia.org/wiki/Voyelle_moyenne_centrale)|
 | i  | i |  v<b>ie</b>| [examples](https://fr.wikipedia.org/wiki/Liste_des_graphies_des_phon%C3%A8mes_du_fran%C3%A7ais#/i/) |[further reading](https://fr.wikipedia.org/wiki/Voyelle_ferm%C3%A9e_ant%C3%A9rieure_non_arrondie)|
 | o  | o | rés<b>eau</b> |[examples](https://fr.wikipedia.org/wiki/Liste_des_graphies_des_phon%C3%A8mes_du_fran%C3%A7ais#/o/)| [further reading](https://fr.wikipedia.org/wiki/Voyelle_mi-ferm%C3%A9e_post%C3%A9rieure_arrondie)|
 | O  | ɔ | s<b>o</b>rt | [examples](https://fr.wikipedia.org/wiki/Liste_des_graphies_des_phon%C3%A8mes_du_fran%C3%A7ais#/%C9%94/)|[further reading](https://fr.wikipedia.org/wiki/Voyelle_mi-ouverte_post%C3%A9rieure_arrondie)|
 | u  | u | f<b>ou</b> | [examples](https://fr.wikipedia.org/wiki/Liste_des_graphies_des_phon%C3%A8mes_du_fran%C3%A7ais#/u/)|[further reading](https://fr.wikipedia.org/wiki/Voyelle_ferm%C3%A9e_post%C3%A9rieure_arrondie)|
 | y  | y | ch<b>u</b>te| [examples](https://fr.wikipedia.org/wiki/Liste_des_graphies_des_phon%C3%A8mes_du_fran%C3%A7ais#/y/)|[further reading](https://fr.wikipedia.org/wiki/Voyelle_ferm%C3%A9e_ant%C3%A9rieure_arrondie)|
 | o~ | ɔ~  | <b>on</b> | [examples](https://fr.wikipedia.org/wiki/Liste_des_graphies_des_phon%C3%A8mes_du_fran%C3%A7ais#/%C9%94%CC%83/)|[further reading](https://fr.wikipedia.org/wiki/Voyelle_moyenne_inf%C3%A9rieure_post%C3%A9rieure_arrondie)- [nasalised](https://fr.wikipedia.org/wiki/Nasalisation)|
 | a~ | &atilde; | b<b>an</b>c | [examples](https://fr.wikipedia.org/wiki/Liste_des_graphies_des_phon%C3%A8mes_du_fran%C3%A7ais#/%C9%91%CC%83/)|[further reading](https://fr.wikipedia.org/wiki/Voyelle_basse_post%C3%A9rieure_non_arrondie)- [nasalised](https://fr.wikipedia.org/wiki/Nasalisation)|
 | e~ | ɛ~ | b<b>ai</b>n| [examples](https://fr.wikipedia.org/wiki/Liste_des_graphies_des_phon%C3%A8mes_du_fran%C3%A7ais#/%C9%9B%CC%83/)|[further reading](https://fr.wikipedia.org/wiki/Voyelle_moyenne_inf%C3%A9rieure_ant%C3%A9rieure_non_arrondie)- [nasalised](https://fr.wikipedia.org/wiki/Nasalisation)|
 | 9~ | œ~ | <b>un</b> | [examples](https://fr.wikipedia.org/wiki/Liste_des_graphies_des_phon%C3%A8mes_du_fran%C3%A7ais#/%C5%93%CC%83/)|[further reading](https://fr.wikipedia.org/wiki/Voyelle_moyenne_inf%C3%A9rieure_ant%C3%A9rieure_arrondie) - [nasalised](https://fr.wikipedia.org/wiki/Nasalisation)|
 

### Semi-Vowels

 | XSAMPA Symbol  | API  | french word | list of words | explanation |          
 |----------------| ------| ---------------|-------------------|-------------------
 | w  |w|  aq<b>ua</b>relle|   [examples](https://fr.wikipedia.org/wiki/Liste_des_graphies_des_phon%C3%A8mes_du_fran%C3%A7ais#/w/)| [further reading](https://fr.wikipedia.org/wiki/Consonne_spirante_labio-v%C3%A9laire_vois%C3%A9e)|
 | j  | j|  b<b>i</b>lle |   [examples](https://fr.wikipedia.org/wiki/Liste_des_graphies_des_phon%C3%A8mes_du_fran%C3%A7ais#/j/)| [further reading](https://fr.wikipedia.org/wiki/Consonne_spirante_palatale_vois%C3%A9e)|
 | H  |ɥ|  h<b>ui</b>t |   [examples](https://fr.wikipedia.org/wiki/Liste_des_graphies_des_phon%C3%A8mes_du_fran%C3%A7ais#/%C9%A5/)| [further reading](https://fr.wikipedia.org/wiki/Consonne_spirante_labio-palatale_vois%C3%A9e)|
 
### Fricatives

 | XSAMPA Symbol  | API  | french word | list of words | explanation |          
 |----------------| ------| ---------------|-------------------|-------------------
 | v |v| a<b>v</b>ion  | [examples](https://fr.wikipedia.org/wiki/Liste_des_graphies_des_phon%C3%A8mes_du_fran%C3%A7ais#/v/)| [further reading](https://fr.wikipedia.org/wiki/Consonne_fricative_labio-dentale_vois%C3%A9e)|
 | z |z| ro<b>s</b>  | [examples](https://fr.wikipedia.org/wiki/Liste_des_graphies_des_phon%C3%A8mes_du_fran%C3%A7ais#/z/)| [further reading](https://fr.wikipedia.org/wiki/Consonne_fricative_alv%C3%A9olaire_vois%C3%A9e)|
 | Z |ʒ| <b>j</b>e  | [examples](https://fr.wikipedia.org/wiki/Liste_des_graphies_des_phon%C3%A8mes_du_fran%C3%A7ais#/%CA%92/)| [further reading](https://fr.wikipedia.org/wiki/Consonne_fricative_post-alv%C3%A9olaire_vois%C3%A9e)|
 | f |f| <b>f</b>eu  | [examples](https://fr.wikipedia.org/wiki/Liste_des_graphies_des_phon%C3%A8mes_du_fran%C3%A7ais#/f/)| [further reading](https://fr.wikipedia.org/wiki/Consonne_fricative_labio-dentale_sourde)|
 | s |s| bro<b>ss</b>ent | [examples](https://fr.wikipedia.org/wiki/Liste_des_graphies_des_phon%C3%A8mes_du_fran%C3%A7ais#/s/)| [further reading](https://fr.wikipedia.org/wiki/Consonne_fricative_alv%C3%A9olaire_sourde)|
 | S |ʃ|  <b>ch</b>at | [examples](https://fr.wikipedia.org/wiki/Liste_des_graphies_des_phon%C3%A8mes_du_fran%C3%A7ais#/%CA%83/)| [further reading](https://fr.wikipedia.org/wiki/Consonne_fricative_post-alv%C3%A9olaire_sourde)|
 
### Plosives

 | XSAMPA Symbol | API| french word | list of words | explanation         
 |---------------| ---| ------------|---------------|------------------- 
 | b |b| ta<b>b</b>le| [examples](https://fr.wikipedia.org/wiki/Liste_des_graphies_des_phon%C3%A8mes_du_fran%C3%A7ais#/b/) | [further reading](https://fr.wikipedia.org/wiki/Consonne_occlusive_bilabiale_vois%C3%A9e)|
 | d |d| <b>d</b>emain  | [examples](https://fr.wikipedia.org/wiki/Liste_des_graphies_des_phon%C3%A8mes_du_fran%C3%A7ais#/d/)| [further reading](https://fr.wikipedia.org/wiki/Consonne_occlusive_alv%C3%A9olaire_vois%C3%A9e)|
 | g |g| ba<b>g</b>ues  | [examples](https://fr.wikipedia.org/wiki/Liste_des_graphies_des_phon%C3%A8mes_du_fran%C3%A7ais#/g/) | [further reading](https://fr.wikipedia.org/wiki/Consonne_occlusive_v%C3%A9laire_vois%C3%A9e)|
 | p |p| <b>p</b>ot   | [examples](https://fr.wikipedia.org/wiki/Liste_des_graphies_des_phon%C3%A8mes_du_fran%C3%A7ais#/p/) | [further reading](https://fr.wikipedia.org/wiki/Consonne_occlusive_bilabiale_sourde)|
 | t |t| comp<b>t</b>er  | [examples](https://fr.wikipedia.org/wiki/Liste_des_graphies_des_phon%C3%A8mes_du_fran%C3%A7ais#/t/) | [further reading](https://fr.wikipedia.org/wiki/Consonne_occlusive_alv%C3%A9olaire_sourde)|
 | k |k| la<b>c</b>  | [examples](https://fr.wikipedia.org/wiki/Liste_des_graphies_des_phon%C3%A8mes_du_fran%C3%A7ais#/k/) | [further reading](https://fr.wikipedia.org/wiki/Consonne_occlusive_v%C3%A9laire_sourde)|

### Nasals

 | XSAMPA Symbol  | API  | french word | list of words | explanation |          
 |----------------| ------| ---------------|-------------------|-------------------
 | m |m| <b>m</b>aison | [examples](https://fr.wikipedia.org/wiki/Liste_des_graphies_des_phon%C3%A8mes_du_fran%C3%A7ais#/m/)| [further reading](https://fr.wikipedia.org/wiki/Consonne_nasale_bilabiale_vois%C3%A9e)|
 | n |n| <b>n</b>ez | [examples](https://fr.wikipedia.org/wiki/Liste_des_graphies_des_phon%C3%A8mes_du_fran%C3%A7ais#/n/)| [further reading](https://fr.wikipedia.org/wiki/Consonne_occlusive_nasale_alv%C3%A9olaire_vois%C3%A9e)|
 | N |ŋ| campi<b>ng</b> | [examples](https://fr.wikipedia.org/wiki/Liste_des_graphies_des_phon%C3%A8mes_du_fran%C3%A7ais#/%C5%8B/)| [further reading](https://fr.wikipedia.org/wiki/Consonne_occlusive_nasale_v%C3%A9laire_vois%C3%A9e)|

### Others

| XSAMPA Symbol  | API  | french word | list of words | explanation |          
|----------------| ------| ---------------|-------------------|-------------------
| R |ʁ| ta<b>r</b>te | [examples](https://fr.wikipedia.org/wiki/Liste_des_graphies_des_phon%C3%A8mes_du_fran%C3%A7ais#/%CA%81/) |[further reading](https://fr.wikipedia.org/wiki/Consonne_fricative_uvulaire_vois%C3%A9e)|
| l |l| <b>l</b>ait |  [examples](https://fr.wikipedia.org/wiki/Liste_des_graphies_des_phon%C3%A8mes_du_fran%C3%A7ais#/l/)|[further reading](https://fr.wikipedia.org/wiki/Consonne_spirante_lat%C3%A9rale_alv%C3%A9olaire_vois%C3%A9e)|
 


## Online resources

To help you to convert a given french text you can make use of online resources. 
because online resources directly using  XSAMPA are hard to find generally you wil have to pass
via the [international phonetic alphabet (IPA)](https://en.wikipedia.org/wiki/International_Phonetic_Alphabet).

### Text to IPA

To get the IPA transcription of a given french word you can either use online dictionaries 
as for example [www. wordreference.com](https://www.wordreference.com/fren/), or even
transcribe a given phrase completely into IPA using
[French to IPA](https://easypronunciation.com/en/french-phonetic-transcription-converter#phonetic_transcription).


### IPA to XSAMPA
 
In a second step you can then use the 
[IPA to SAMPA converter](http://www.lfsag.unito.it/ipa/converter_en.html)
to get the transcription in the SAMPA alphabet, which for the part of the IPA that is covered by _ISiS_ 
corresponds more or less with the XSAMPA implemented in _ISiS_.
The only difference are the nasalised vowels that in _ISiS_ are always lower-case, while in the 
SAMPA transcription produced by means of [IPA to SAMPA converter](http://www.lfsag.unito.it/ipa/converter_en.html)
there is a difference between e~ ad E~. So when constructing the _ISiS_ score you
should convert all nasalised vowels, that are letters with a "~" attached, from upper-case into the
corresponding lower-case letter.
  
