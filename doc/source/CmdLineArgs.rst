The ISiS command line arguments
-------------------------------

Flags and flag variants
~~~~~~~~~~~~~~~~~~~~~~~

Most of the ISiS command line flags have two variants: a short and a
long form. The long form is given by means of two dashes "--" while the
short form is introduced by a single dash '-'. We take as example the
flag that requests automatically opening the synthesized sound and the
related pitch contour and phoneme sequence. In the introductory section
we have learned that this can be achieved by adding `-a` to the
command. This is the short form "-a", and equivalently we could have
given the long form --auto\_open as argument. So the following two
commands

.. code:: bash

    $> isis.sh -sv EL -o defsong_EL.wav -a

.. code:: bash

    $> isis.sh -sv EL -o defsong_EL.wav --auto_open 

are strictly equivalent. In the following we will generally introduce
only the long form of the parameter, as for example `(--auto_open)`.
The long form is supposed to be easier to remember as it declares its
function by means of its name. However, in case we want to directly
describe both forms we will use a vertical bar to separate both forms
`(-a| --auto_open)`.

Finally, ISiS supports short cutting the long flag to arbitrary shorter
lengths, as long as the flag remains unambiguous. So another equivalent
command line would be:

.. code:: bash

    $> isis.sh -sv EL -o defsong_EL.wav --auto 

Help
~~~~

One of the most important flags for ISiS is the help flag
`(-h|--help)`, which can be sued to list all command line flags with a
short help text.

To see the full list of command line argument of ISiS you can therefore
simply run

.. code:: bash

    $> isis.sh -h

which will display a short usage summary, then the program version, and
finally a long help section listing all flags in short and long form,
together with default values, that here are given for the case that ISiS
version 1.2.5 installed under /Users/me/Applications/ISiS\_V1.2.5:

.. raw:: html

   <pre><code>
   usage: ISiS [-h] [-v] [-m MELODY] [-o OUTFILE] [-r CORPORA_ROOT]
               [-sv SINGING_VOICE] [-ss SINGING_STYLE] [-nls | -kol] [-nps]
               [-pls PHON_LEN_STYLE] [-gt GLOBAL_TRANSP] [-te TEMPO]
               [-a | -A | -O] [-pp] [-q] [-sr STYLES_ROOT]
               [--cfg_synth CFG_SYNTH] [--cfg_style CFG_STYLE]
               [--temproot TEMPROOT]

   ISiS - IRCAM singing synthesis (Version 1.2.5)

   optional arguments:
     -h, --help            show this help message and exit
     -v, --version         show program's version number and exit

   ISiS IO:
     -m MELODY, --melody MELODY
                           config file providing score, note, lyrics and loudness
                           information (Def: read from configSynth.cfg file)
     -o OUTFILE, --outfile OUTFILE
                           filename of the synthesized output sound file (Def:
                           read from config_files/tempFiles.cfg in the chanter
                           modules root directory)

   ISiS voice:
     -r CORPORA_ROOT, --corpora_root CORPORA_ROOT
                           root directory containing singing voice databases, set
                           default via environment variable ISIS_CORPORA (Def:
                           /Data/Corpora/BDD)
     -sv SINGING_VOICE, --singing_voice SINGING_VOICE
                           directory within ISIS_CORPRORA of the singing database
                           to be used for synthesis (Def: $ISIS_VOICE = RT/RT_YM)

   ISiS style:
     -ss SINGING_STYLE, --singing_style SINGING_STYLE
                           select singing style, one of eP, jG, fL, sD, None
                           (Def: $ISIS_STYLE=None)
     -nls, --no_loudness_style_model
                           disable context aware loudness style models and use
                           only the default model (Def: False)
     -kol, --keep_orig_loudness
                           disable all loudness style models and keep the
                           original loudness contours of the singing db (Def:
                           False)
     -nps, --no_f0_style_model
                           disable pitch contour models (Def: False)
     -pls PHON_LEN_STYLE, --phon_len_style PHON_LEN_STYLE
                           select phon len style, one of eP, jG, fL, sD, meta,
                           None (Def: $ISIS_STYLE=None)
     -gt GLOBAL_TRANSP, --global_transp GLOBAL_TRANSP
                           global transposition in midi notes (Def: 0)
     -te TEMPO, --tempo TEMPO
                           tempo in bpm, will override the value written in the
                           score (Def: None)

   ISiS opts:
     -a, --auto_open       force opening the result in AS (Def:
                           cfg_synth::auto_open parameters)
     -A, --no_auto_open    force not opening the result in AS even if config
                           files demands to open results (Def:
                           cfg_synth::auto_open parameter)
     -O, --Open            open the result in the default application for sound
                           files (Def: False)
     -pp, --pan_parts      output separate voiced and unvoiced singing signal
                           next to the output file (Def: False)
     -q, --quiet           suppress logging to console (Def: False)

   ISiS advanced parameters:
     -sr STYLES_ROOT, --styles_root STYLES_ROOT
                           root directory for style model files (Def: /Users/me/
                           Applications/ISiS_V1.2.5/config_files/styles)
     --cfg_synth CFG_SYNTH
                           Synthesis configuration file (Def:
                           %(ISIS_CONFIG_DIR)s/default.configSynth.cfg)
     --cfg_style CFG_STYLE
                           Singing style configuration file (Def:
                           %(ISIS_CONFIG_DIR)s/default.configStyle.cfg)
     --temproot TEMPROOT   directory for storing temporary files (Def:
                           cfg_synth::TEMPFILESPATH)
   </pre>

The command line arguments are grouped into sections that affect
different aspects of the synthesis. We will discuss these sections in
the the following paragraphs

ISiS IO
~~~~~~~

The IO parameters specify input and output files,

-  **flag --melody**: provides the name of the :ref:`isis-score-files`
   that contains the textual description of melody and phonemes.

-  **flag --outfile**: the name of the output file. The file format of
   the output file is AIFF format, however the format will be adapted
   automatically to the output file extension.

The following sndfile formats will be used depending on the extension.

+--------------+-----------------+
| extension    | Fileformat      |
+==============+=================+
| .au          | SUN AU format   |
+--------------+-----------------+
| .wav         | MS WAV          |
+--------------+-----------------+
| .caf         | Apple CAF       |
+--------------+-----------------+
| .aif/.aiff   | SGI AIFF        |
+--------------+-----------------+

ISiS Voice
~~~~~~~~~~

The voice parameters select the singing voice that is used. The singing
voices that are currently available for ISiS are described in `the
installation instructions <voice_installation.html#ISiS_databases>`__.

-  | **flag --corpora\_root**: overrides the $ISIS\_CORPPORA environment
     variable you should have created when installing the singing voices
     as explained in the section on
     :ref:`singing databases <install-voice-database>`. This parameter
     is not very important if you followed the advice to put all singing
     voices into the same directory and you have correctly initialized
     the ISIS\_CORPORA environment variable (see
   | :ref:`configure ISIS_CORPORA variable <configure-isis-corpora-root-dir>`).

-  **flag --singing\_voice**: this flag on the other hand allows you
   switching voices for each individual synthesis. this flag obtains its
   default from the ISIS\_VOICE environment variable that you can
   position by means of running the install as default voice app in each
   voice directory.

ISiS Style
~~~~~~~~~~

Singing style in ISiS can be controlled on various levels. The basic
command line style parameters allow selecting basic style models, and as
well as controlling certain aspects of the use of style models. A
singing style in ISiS is a model that controls the singing parameter
contours pitch and intensity, as well as the duration of the different
phonemes, depending on the musical context (note frequencies, and note
durations). There are 5 preconfigured singing style models available in
ISiS: these are eP, jG, fL, sD, and finally None.

The style models have been defined with the help of a musicologist, the
None style simply uses default settings of the pitch style parameters,
and intensity contours from the singing voice databases.

-  **flag --singing\_style**: selects a singing style model from the
   five available models that are named: eP, jG, fL, sD, None. The
   default style is derived from the environment variable ISIS\_STYLE,
   and if that is not set, then default style None is used.

-  **flag --no\_loudness\_style\_model**: exclude note attack and
   release intensity contours from the effect of the selected style
   model (Def: False).

-  **flag --no\_f0\_style\_model**: exclude pitch contour parameters
   (note attack, release, note transitions, vibrato parameters) from the
   effect of the selected style model (Def: False)

-  **flag --global\_transp**: note quite the same level as the other
   styme parameters, here a global transposition in midi notes can be
   applied to the score (Def: 0)

ISiS opts
~~~~~~~~~

A few options processing options are available.

-  **flag --auto\_open**: triggers opening the resulting synthesis file
   in the AudioSculpt application. The sound file will be opened with
   phoneme annotations and the generated pitch contour. THe default
   behavior is to respect the corresponding setting in the
   default.configSynth parameter.

-  **flag--no\_auto\_open**: prevents opening the synthesis results
   result in AudioSculpt even even if the auto\_open parameter in
   default.configSynth.cfg file is set.
-  **flag --Open**: open the synthesis result in the default application
   for sound files (Def: False)
-  **flag --pan\_parts**: output separate voiced and unvoiced singing
   signal next to the output file (Def: False)
-  **flag --quiet**: suppress logging of progress messages to the
   terminal console (Def: False)

ISiS advanced parameters
~~~~~~~~~~~~~~~~~~~~~~~~

The following advanced parameter can be used to control more aspects of
the singing synthesiser.

**TODO: add description**\ \*

-  **flag --cfg\_style**: Singing style configuration file (Def:
   default.configStyle.cfg)
-  **flag --cfg\_synth**: Synthesis configuration file
-  **flag --styles\_root**: root directory for style model files (Def:
   default.configSytnh.cfg
-  **flag --temproot**: directory for storing temporary intermediate
   files files (Def: cfg\_synth::TEMPFILESPATH)
