#! /usr/bin/env python

import os
from argparse import ArgumentParser

if __name__ == "__main__" :

    parser = ArgumentParser(description="filter pandoc generated files to produce valid rst files")

    parser.add_argument("infile", help="input rst file with remaining markdown escapes" )
    parser.add_argument("outfile", help="output rst file with markdown escapes removed" )

    args = parser.parse_args()

    with open(args.infile, "r") as fi:
        with open(args.outfile, "w") as fo:
            for line in fi:
                if line.startswith(".. toctreeinsert::"):
                    with open(os.path.join(os.path.dirname(args.infile),
                                           line.replace(".. toctreeinsert::", "").strip()), "r") as ft:
                        for ttt in ft:
                            fo.write(ttt)
                else:
                    fo.write(line.replace(".. \\_", ".. _").replace("``", "`"))
